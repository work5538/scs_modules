import time
import boto3
import json
import threading
import logging
import signal
from botocore.client import Config
from botocore.vendored.requests.exceptions import ReadTimeout
from multiprocessing import Process

# Logger Settings
# boto3.set_stream_logger('')
logger = logging.getLogger(__name__)
console_h= logging.StreamHandler()
logger.addHandler(console_h)
logger.setLevel(logging.INFO)

# AWS Config
# config_dict = {'region_name': 'us-east-1', 'connect_timeout': 65, 'read_timeout': 65, 'retries': {'max_attempts': 0}}
config_dict = {'region_name': 'us-east-1'}
config = Config(**config_dict)
# session = boto3.session.Session()
client = boto3.client('stepfunctions', config=config)

# Shutdown logic
def handler_stop_signals(signum, frame):
    logger.info("stopping")
    p.terminate()

signal.signal(signal.SIGINT, handler_stop_signals)

# Class 
class Worker:

    def __init__(self):
        self.processing = False
        self.token = ""

    def start(self):
        logger.info("connected")
                
        while True:
            try:                                                     
                self.getActivity()                
                self.process()                
                self.respondSuccess()                                                

            except ReadTimeout as e:
                logger.info(e)
                self.processing = False
                time.sleep(5)

    def respondSuccess(self):   
        logger.info("completed successfully")
        client.send_task_success(
            taskToken=self.token,
            output='{}'
        )    

    def process(self):
        logger.info("processing")
        self.processing = True

        try:
            threading.Thread(target=self.heartbeat).start()
        except Exception as e:
            logger.info("failed to spawn heartbeat: "+str(e))
        
        #todo: do some work
        logger.info("isProcessing: "+str(self.processing))
        time.sleep(30)

        self.processing = False

    def getActivity(self):
        logger.info("waiting")
        response = client.get_activity_task(
            activityArn='arn:aws:states:us-east-1:210811600188:activity:rtp-openstack-echo',
            workerName='echo-worker'
        )        
        self.token = response['taskToken']

    def heartbeat(self):        
        while self.processing:
            client.send_task_heartbeat(
                taskToken=self.token
            )
            logger.info("heart is beating")
            time.sleep(10)
    
#Main function 
if __name__ == '__main__':
    p = Process(target=Worker().start)
    p.start()
    p.join()